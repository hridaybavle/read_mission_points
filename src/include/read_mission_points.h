//I/O stream
//std::cout
#include <iostream>
#include <fstream>
#include <numeric>
#include "mutex"
#include "stdio.h"
#include "iomanip"

#include "ros/ros.h"
#include "robot_process.h"

#include "droneMsgsROS/droneStatus.h"
#include "droneMsgsROS/droneCommand.h"
#include "drone_utils/drone_state_enum.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "geometry_msgs/Vector3Stamped.h"

//dji sdk
#include "dji_sdk/Gimbal.h"
#include "dji_sdk/CameraAction.h"

//ros messages
#include <sensor_msgs/PointCloud2.h>

#include <aerostack_msgs/ProcessState.h>

//pcl headers
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_msgs/ModelCoefficients.h>

class read_mission : public RobotProcess
{
public:
    //constructor and destructor
    read_mission();
    ~read_mission();

public:
    void ownSetUp();
    void ownStart();
    void ownStop();
    void ownRun();
    void init();

    ros::NodeHandle n;
public:
    void initParams();
    void readPoseDataFromText();

protected:
    ros::Subscriber get_drone_status_;
    void getDroneStatusCallback(const droneMsgsROS::droneStatus& msg);

    ros::Subscriber get_drone_estimated_pose_;
    void getDroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg);

    ros::Subscriber get_point_cloud_msgs_;
    void getPointCloudMsgCallback(const sensor_msgs::PointCloud2ConstPtr& msg);
		
    ros::Subscriber get_gimbal_angle_;
    void gimbalAngleCallback(const geometry_msgs::Vector3Stamped& msg);			
	
    ros::Publisher drone_high_level_command_pub_;
    void publishDroneHighLevelCommand(droneMsgsROS::droneCommand drone_command_msg);

    ros::Publisher drone_abs_trajectory_command_pub_;
    void PublishDroneAbsTrajectoryCommand(droneMsgsROS::dronePositionRefCommand trajectory_point);

    ros::Publisher drone_yaw_reference_command_pub_;
    void PublishDroneYawRefCommand(double yaw_command);

    ros::Publisher gimbal_angle_pub_;

    //services
    ros::ServiceClient start_trajectory_controller_;
    ros::ServiceClient camera_action_control_start_srv_;
    ros::ServiceClient camera_action_start_srv_;
    ros::ServiceClient gimbal_action_start_srv_;

    void record_photo_with_text(double current_pitch_comand);
    void PublishCamPitchYawCommand(double pitch_command, double yaw_command);


private:
    std::ofstream flight_data_;
    std::string current_date_time_;
    const std::string currentDateTime();
    int dji_photo_counter_;

    std::string text_file_;
    std::string result_file_;

    double obstacle_threshold_;
    std::vector<double> average_min_distance_vec_;

    double x_laser_cam_trans_, y_laser_cam_trans_, z_laser_cam_trans_;

private:
    droneMsgsROS::droneStatus current_drone_status_;
    droneMsgsROS::droneCommand drone_command_msgs_;
    bool first_take_off_;
    bool trajectory_point_sent_;
    bool first_trajectory_point_sent_;
    bool yaw_ref_published_;
    bool photo_taken_;
    bool yaw_point_reached_;
    int trajectory_point_num_;
    bool stop_trajectory_;
    bool activate_obstacle_mode_;
    //DroneState::ModeType drone_mode_;

    droneMsgsROS::dronePose last_estimated_pose_;
		float gimbal_yaw_, gimbal_pitch_, init_yaw_;
		bool init_yaw_received_;

public:
    struct trajectory_points
    {
        double x;
        double y;
        double z;
        double yaw;
        double pitch;
				double cam_yaw;
        double velocity;
        double comments;
    };

    std::vector<trajectory_points> trajectory_points_vec_;

};

