#include "read_mission_points.h"

read_mission::read_mission():RobotProcess()
{
    std::cout << "started node for reading mission data" << std::endl;
}

read_mission::~read_mission()
{
    std::cout << "shutting down the node for reading mission data " << std::endl;
}

void read_mission::ownSetUp()
{
    this->initParams();
    this->readPoseDataFromText();

}

void read_mission::initParams()
{

    first_take_off_              = false;
    trajectory_point_sent_       = false;
    first_trajectory_point_sent_ = false;
    yaw_point_reached_           = false;
    yaw_ref_published_           = false;
    photo_taken_                 = false;
    stop_trajectory_             = false;
    activate_obstacle_mode_      = false;
    init_yaw_received_           = false;

    init_yaw_			    = 0;
    trajectory_point_num_   = 0;
    x_laser_cam_trans_ = 0; y_laser_cam_trans_ = 0; z_laser_cam_trans_ = 0;
    average_min_distance_vec_.clear();
    gimbal_yaw_ = 0; gimbal_pitch_ = 0;

    ros::param::get("~dji_photo_counter_", dji_photo_counter_);
    std::cout << "Dji photo counter is: " << dji_photo_counter_ << std::endl;

    ros::param::get("~text_file", text_file_);
    if(text_file_.length() == 0)
        text_file_ = "/home/hriday/trajectory_points.txt";
    std::cout << "text file " << text_file_ << std::endl;

    ros::param::get("~result_file", result_file_);
    if(result_file_.length() == 0)
        result_file_ = "/home/hriday";
    std::cout << "result file " << result_file_ << std::endl;
    
    ros::param::get("~obstacle_threshold", obstacle_threshold_);
    if(obstacle_threshold_ == 0)
        obstacle_threshold_ = 1.50;
    std::cout << "obstacle threshold " << obstacle_threshold_ << std::endl;

    ros::param::get("~x_laser_cam_trans", x_laser_cam_trans_);
    if(x_laser_cam_trans_ == 0)
        x_laser_cam_trans_ = -0.11;
    std::cout << "x_laser_cam_trans  " << x_laser_cam_trans_ << std::endl;

    ros::param::get("~y_laser_cam_trans", y_laser_cam_trans_);
    if(y_laser_cam_trans_ == 0)
        y_laser_cam_trans_ = -0.085;
    std::cout << "y_laser_cam_trans  " << y_laser_cam_trans_ << std::endl;

    ros::param::get("~z_laser_cam_trans", z_laser_cam_trans_);
    if(z_laser_cam_trans_ == 0)
        z_laser_cam_trans_ = 0.035;
    std::cout << "z_laser_cam_trans " << z_laser_cam_trans_ << std::endl;

}

void read_mission::readPoseDataFromText()
{
    std::ifstream infile(text_file_);
    if(!infile.is_open())
    {
        std::cerr << "cannot open the pose file " << std::endl;
    }


    trajectory_points points;
    std::string line;
    bool first_line=true;
    while(std::getline(infile, line))
    {
        if(first_line)
        {
            int airplane_code = std::stoi(line);
            first_line = false;
            std::cout << "airplane code " << airplane_code << std::endl;
            continue;
        }

        std::string x, y,z, yaw, pitch, cam_yaw;

        std::stringstream ss(line);
        //frame of references:
        //yaw drone to the right -ve, pitch cam bottom -ve, yaw cam right +ve
        std::getline(ss, x, ',');
        points.x = (std::stof(x)/1000.0) + x_laser_cam_trans_;
        std::getline(ss, y, ',');
        points.y = (std::stof(y)/1000.0) + y_laser_cam_trans_;
        std::getline(ss, z, ',');
        points.z = (std::stof(z)/1000.0) + z_laser_cam_trans_;
        std::getline(ss, yaw, ',');
        points.yaw = std::stof(yaw);
        std::getline(ss, pitch, ',');
        points.pitch = std::stof(pitch);
        std::getline(ss, cam_yaw, ',');
        points.cam_yaw = std::stof(cam_yaw);

        //converting yaw from radians to degrees and converting according to aerostack frame
        points.yaw      = (-1)*points.yaw;
        points.pitch    = (-1)*points.pitch;

        std::cout << " X: " << points.x  << " Y: " << points.y  << " Z: " << points.z  << std::endl;
        std::cout << " Yaw: " << points.yaw << " Camera Pitch: " << points.pitch << " Camera Yaw: " << cam_yaw << std::endl;

        trajectory_points_vec_.push_back(points);
    }

}


void read_mission::ownStop()
{

}

void read_mission::ownRun()
{
    //std::cout << "sending the mission " << std::endl;
    //taking off
    if(this->getState() == aerostack_msgs::ProcessState::NotStarted)
        return;
    
    if((current_drone_status_.status == droneMsgsROS::droneStatus::LANDED || current_drone_status_.status == droneMsgsROS::droneStatus::UNKNOWN)
            && !first_take_off_)
    {
        drone_command_msgs_.command = droneMsgsROS::droneCommand::TAKE_OFF;
        first_take_off_ = true;
        publishDroneHighLevelCommand(drone_command_msgs_);
    }

    //starting the controller
    if(current_drone_status_.status == droneMsgsROS::droneStatus::HOVERING && first_take_off_)
    {
        drone_command_msgs_.command = droneMsgsROS::droneCommand::MOVE;
        publishDroneHighLevelCommand(drone_command_msgs_);

        std_srvs::Empty trajectory_empty_srv_msg;
        start_trajectory_controller_.call(trajectory_empty_srv_msg);

    }

    //checking for obstacles
    if(stop_trajectory_ && current_drone_status_.status == droneMsgsROS::droneStatus::FLYING && first_trajectory_point_sent_)
    {
        if(!activate_obstacle_mode_)
        {
            std::cout << "sending the last position as obstacle in range " << std::endl;
            droneMsgsROS::dronePositionRefCommand last_waypoint;
            last_waypoint.x = last_estimated_pose_.x;
            last_waypoint.y = last_estimated_pose_.y;
            last_waypoint.z = last_estimated_pose_.z;

            this->PublishDroneAbsTrajectoryCommand(last_waypoint);
            activate_obstacle_mode_ = true;
        }

    }
    if(activate_obstacle_mode_ && !stop_trajectory_ && current_drone_status_.status == droneMsgsROS::droneStatus::FLYING)
    {
        std::cout << "continuing with the mission as no obstacle in the range" << std::endl;
        droneMsgsROS::dronePositionRefCommand last_waypoint;
        last_waypoint.x = trajectory_points_vec_[trajectory_point_num_-1].x;
        last_waypoint.y = trajectory_points_vec_[trajectory_point_num_-1].y;
        last_waypoint.z = trajectory_points_vec_[trajectory_point_num_-1].z;

        this->PublishDroneAbsTrajectoryCommand(last_waypoint);
        activate_obstacle_mode_ = false;
    }


    //if no obstacles sending the trajectory points
    if(current_drone_status_.status == droneMsgsROS::droneStatus::FLYING && first_take_off_ && !activate_obstacle_mode_)
    {
        //publish the points from the text file
        if(trajectory_point_num_ < trajectory_points_vec_.size())
        {

            //----- sending the first point---/////
            if(!first_trajectory_point_sent_)
            {

                //first publishing the yaw of the point
                if(fabs(last_estimated_pose_.yaw - trajectory_points_vec_[trajectory_point_num_].yaw) > 0.1)
                {
                    if(!yaw_ref_published_)
                    {
                        std::cout << "turning in yaw " << std::endl;
                        this->PublishDroneYawRefCommand(trajectory_points_vec_[trajectory_point_num_].yaw);
                    }

                }
                else
                {
                    std::cout << "turned in yaw, now sending the first point " << std::endl;
                    droneMsgsROS::dronePositionRefCommand first_waypoint;
                    first_waypoint.x = trajectory_points_vec_[trajectory_point_num_].x;
                    first_waypoint.y = trajectory_points_vec_[trajectory_point_num_].y;
                    first_waypoint.z = trajectory_points_vec_[trajectory_point_num_].z;

                    this->PublishDroneAbsTrajectoryCommand(first_waypoint);
                    this->PublishCamPitchYawCommand(trajectory_points_vec_[trajectory_point_num_].pitch,
                                                    trajectory_points_vec_[trajectory_point_num_].cam_yaw);
                    //this->record_photo_with_text();
                    trajectory_point_num_++;
                    first_trajectory_point_sent_ = true;
                    //yaw_point_reached_ = false;
                    yaw_ref_published_ = false;
                }
            }


            //----- sending the next points with yaw reference command first---/////
            if (fabs(last_estimated_pose_.x - trajectory_points_vec_[trajectory_point_num_-1].x) < 0.30 &&
                    fabs(last_estimated_pose_.y - trajectory_points_vec_[trajectory_point_num_-1].y) < 0.30 &&
                    fabs(last_estimated_pose_.z - trajectory_points_vec_[trajectory_point_num_-1].z) < 0.30 &&
                    (fabs(trajectory_points_vec_[trajectory_point_num_-1].pitch) - fabs(gimbal_pitch_)) < 0.3 &&
                    (fabs(trajectory_points_vec_[trajectory_point_num_-1].cam_yaw) - fabs(gimbal_yaw_)) < 0.3 &&
                    first_trajectory_point_sent_)
            {
                //recording the data and taking the photo at the current point
                //std::cout << "commanded cam yaw " << trajectory_points_vec_[trajectory_point_num_-1].cam_yaw << std::endl;
                //std::cout << "estimated cam yaw " << gimbal_yaw_ << std::endl;

                if(!photo_taken_)
                    this->record_photo_with_text(trajectory_points_vec_[trajectory_point_num_-1].pitch);

                //first publishing the yaw of the nexy point
                if(fabs(last_estimated_pose_.yaw - trajectory_points_vec_[trajectory_point_num_].yaw) > 0.1)
                {
                    if(!yaw_ref_published_)
                    {
                        std::cout << "turning in yaw " << std::endl;
                        this->PublishDroneYawRefCommand(trajectory_points_vec_[trajectory_point_num_].yaw);
                    }

                }
                else
                {
                    std::cout << "turned in yaw, now sending the next points " << std::endl;
                    //sending the x,y,z points
                    std::cout << "sending the next point " << std::endl;
                    droneMsgsROS::dronePositionRefCommand next_waypoint;
                    next_waypoint.x = trajectory_points_vec_[trajectory_point_num_].x;
                    next_waypoint.y = trajectory_points_vec_[trajectory_point_num_].y;
                    next_waypoint.z = trajectory_points_vec_[trajectory_point_num_].z;

                    PublishDroneAbsTrajectoryCommand(next_waypoint);
                    this->PublishCamPitchYawCommand(trajectory_points_vec_[trajectory_point_num_].pitch,
                                                    trajectory_points_vec_[trajectory_point_num_].cam_yaw);
                    trajectory_point_num_++;
                    //yaw_point_reached_ = false;
                    yaw_ref_published_ = false;
                    photo_taken_       = false;
                }

            }
        }
        else
        {
            if (fabs(last_estimated_pose_.x - trajectory_points_vec_[trajectory_point_num_-1].x) < 0.30 &&
                    fabs(last_estimated_pose_.y - trajectory_points_vec_[trajectory_point_num_-1].y) < 0.30 &&
                    fabs(last_estimated_pose_.z - trajectory_points_vec_[trajectory_point_num_-1].z) < 0.30 &&
                    (fabs(trajectory_points_vec_[trajectory_point_num_-1].pitch) - fabs(gimbal_pitch_)) < 0.3 &&
                    (fabs(trajectory_points_vec_[trajectory_point_num_-1].cam_yaw) - fabs(gimbal_yaw_)) < 0.3 &&
                    first_trajectory_point_sent_)
            {
                //recording the data and taking the photo at the current point
                if(!photo_taken_)
                    this->record_photo_with_text(trajectory_points_vec_[trajectory_point_num_-1].pitch);

                drone_command_msgs_.command = droneMsgsROS::droneCommand::LAND;
                publishDroneHighLevelCommand(drone_command_msgs_);
                std::cout << "finished the mission" << std::endl;
                std::cout << "dji photo counter is " << dji_photo_counter_ << std::endl;

            }
        }

    }
}

void read_mission::ownStart()
{
    //subscribers
    get_drone_status_           = n.subscribe("status",1, &read_mission::getDroneStatusCallback, this);
    get_drone_estimated_pose_   = n.subscribe("EstimatedPose_droneGMR_wrt_GFF", 1, &read_mission::getDroneEstimatedPoseCallback, this);
    get_point_cloud_msgs_       = n.subscribe("/filtered_points", 1, &read_mission::getPointCloudMsgCallback, this);
    get_gimbal_angle_		    = n.subscribe("dji_sdk/gimbal_angle", 1, &read_mission::gimbalAngleCallback, this);

    //publishers
    drone_high_level_command_pub_      = n.advertise<droneMsgsROS::droneCommand>("command/high_level", 1);
    drone_abs_trajectory_command_pub_  = n.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>("droneTrajectoryAbsRefCommand",1);
    drone_yaw_reference_command_pub_   = n.advertise<droneMsgsROS::droneYawRefCommand>("droneControllerYawRefCommand",1);
    gimbal_angle_pub_                  = n.advertise<dji_sdk::Gimbal>("dji_sdk/gimbal_angle_cmd",1);


    //services
    start_trajectory_controller_       = n.serviceClient<std_srvs::Empty>("droneTrajectoryController/start");
    camera_action_start_srv_           = n.serviceClient<dji_sdk::CameraAction>("dji_sdk/camera_action");

    current_date_time_ = this->currentDateTime();
    usleep(1e6);
}

void read_mission::getDroneStatusCallback(const droneMsgsROS::droneStatus &msg)
{
    current_drone_status_ = msg;
    return;
}


void read_mission::publishDroneHighLevelCommand(droneMsgsROS::droneCommand drone_command_msg)
{
    drone_high_level_command_pub_.publish(drone_command_msg);

}

void read_mission::getDroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg)
{
    last_estimated_pose_ = msg;

}

void read_mission::gimbalAngleCallback(const geometry_msgs::Vector3Stamped& msg)
{
    gimbal_pitch_ = msg.vector.y * (M_PI/180.0);

    if(!init_yaw_received_)
    {
        init_yaw_ = msg.vector.z;
        init_yaw_received_ = true;
    }
    //std::cout << "init yaw " << init_yaw_ << std::endl;

    float yaw  = msg.vector.z;
    if(yaw - init_yaw_ < -180.0)
        yaw = (yaw - init_yaw_) + 360;
    else if(yaw - init_yaw_ > 180)
        yaw = (yaw - init_yaw_) - 360;
    else
        yaw = yaw - init_yaw_;

    gimbal_yaw_ = yaw * (M_PI/180.0);
    //std::cout << "final yaw: " << gimbal_yaw_ << std::endl;

    return;

}

void read_mission::getPointCloudMsgCallback(const sensor_msgs::PointCloud2ConstPtr &msg)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_n_ (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg(*msg, *cloud_n_);

    std::vector<double> distance_vec;

    distance_vec.clear();
    //calculate the L1 norm of the points x,y and z
    for(size_t i = 0; i < cloud_n_->size(); ++i)
    {
        double distance  = cloud_n_->points[i].getVector3fMap().norm();
        distance_vec.push_back(distance);
    }

    auto min_distance = *std::min_element(distance_vec.begin(),distance_vec.end());

    average_min_distance_vec_.push_back(min_distance);

    if(average_min_distance_vec_.size() == 10)
    {
        double avg_mean_distance;
        avg_mean_distance = std::accumulate(average_min_distance_vec_.begin(), average_min_distance_vec_.end(), 0.0)/average_min_distance_vec_.size();

        //std::cout << "distance  threshold" << obstacle_threshold_  << std::endl;
        //std::cout << "min distance " << avg_mean_distance  << std::endl;

        if(avg_mean_distance < obstacle_threshold_)
        {
            std::cout<< "obstacle in the range stopping trajectory " << std::endl;
            stop_trajectory_ = true;
        }
        else
        {
            stop_trajectory_ = false;
            //std::cout << "no obstalces in range " << std::endl;
        }
        average_min_distance_vec_.clear();
    }
}


void read_mission::PublishDroneAbsTrajectoryCommand(droneMsgsROS::dronePositionRefCommand trajectory_point)
{
    droneMsgsROS::dronePositionTrajectoryRefCommand single_trajectory_point;
    single_trajectory_point.header.stamp = ros::Time::now();
    single_trajectory_point.droneTrajectory.push_back(trajectory_point);

    drone_abs_trajectory_command_pub_.publish(single_trajectory_point);
}


void read_mission::PublishDroneYawRefCommand(double yaw_command)
{
    droneMsgsROS::droneYawRefCommand drone_yaw_reference;
    drone_yaw_reference.header.stamp = ros::Time::now();
    drone_yaw_reference.yaw = yaw_command;

    drone_yaw_reference_command_pub_.publish(drone_yaw_reference);
    yaw_ref_published_ = true;

}

void read_mission::PublishCamPitchYawCommand(double pitch_command, double yaw_command)
{
    //multiplying the pitch command by
    dji_sdk::Gimbal gimbal_angle;

    //absolute command
    gimbal_angle.mode = 1;
    gimbal_angle.ts   = 2;
    gimbal_angle.pitch = pitch_command;
    gimbal_angle.yaw   = yaw_command;

    gimbal_angle_pub_.publish(gimbal_angle);

    //usleep(1000000);
}

void read_mission::record_photo_with_text(double current_pitch_comand)
{
    //taking the photo
    dji_sdk::CameraAction cameraAction;
    cameraAction.request.camera_action = 0;
    camera_action_start_srv_.call(cameraAction);
    usleep(1e6);
    camera_action_start_srv_.call(cameraAction);

    if(cameraAction.response.result == true)
    {
        std::cout << "photo taken" << std::endl;

        current_date_time_ = this->currentDateTime();

        //converting to degrees
        double required_yaw = -(1)*last_estimated_pose_.yaw;

        std::stringstream ss;
        ss << result_file_ << "DJI_" << std::setw(4) << std::setfill('0') << dji_photo_counter_ << ".txt";
        std::string result_file_path;
        result_file_path =  ss.str();

        //creating the results.csv text file
        flight_data_.open (result_file_path, std::ios::out) ;
        //flight_data_ << "Date Time,X,Y,Z,yaw [rad],pitch [rad]" << std::endl;
        flight_data_.close();

        //writing the positions on the results file
        flight_data_.open (result_file_path, std::ios::out);
        flight_data_ << current_date_time_ << ","
                     << (last_estimated_pose_.x - x_laser_cam_trans_)  * (1000) << "," << (last_estimated_pose_.y - y_laser_cam_trans_) * (1000) << "," << (last_estimated_pose_.z - z_laser_cam_trans_) * (1000) << ","
                     << required_yaw << "," << gimbal_pitch_ << "," << gimbal_yaw_ << std::endl;
        flight_data_.close();
        photo_taken_ = true;
        dji_photo_counter_ += 2;
        //usleep(1000000);
    }
    else
    {
        std::cout << "error taking photo" << std::endl;
    }

}

const std::string read_mission::currentDateTime()
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}

