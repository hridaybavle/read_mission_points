#include "read_mission_points.h"

int main(int argc,char **argv)
{

    ros::init(argc, argv, ros::this_node::getName());

    read_mission read_mission_obj_;
    read_mission_obj_.setUp();


    ros::Rate r(10);

    while(ros::ok())
    {
        //updating all the ros msgs
        ros::spinOnce();
        //running the localizer
        read_mission_obj_.run();
        r.sleep();
    }


    return 1;
}

